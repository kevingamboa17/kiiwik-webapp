import AdvertisementsFeed from './scenes/advertisements/scenes/advertisementsFeed/AdvertisementsFeed.vue';
import NegotiationsSection from './scenes/negotiations/scenes/Negotiations/NegotiationsContainer.vue';
import Chat from './scenes/negotiations/scenes/Messages/Chat.vue';
import Settings from './scenes/settings/Settings.vue';
import SingleAdvertisement from './scenes/advertisements/scenes/singleAdvertisement/SingleAdvertisement.vue';
import MainBottomNavigation from './components/MainBottomNavigation.vue';
import AdvertisementCreation from './scenes/myStore/publishAd/AdvertisementCreation.vue';
import MyStore from './scenes/myStore/MyStore.vue';
import Login from './scenes/sign/Login/Login.vue';
import LoginRequired from './components/LoginRequiredScreen.vue';

import { ROUTES } from './utils/Constants';

export default [
  {
    path: '/',
    redirect: ROUTES.SECTIONS.ADVERTISEMENTS,
    component: MainBottomNavigation,
    children: [
      {
        path: ROUTES.SECTIONS.ADVERTISEMENTS,
        component: AdvertisementsFeed,
      }, {
        path: ROUTES.SECTIONS.NEGOTIATIONS,
        component: NegotiationsSection,
        meta: { requiresAuth: true },
      }, {
        path: ROUTES.SECTIONS.SETTINGS,
        component: Settings,
        meta: { requiresAuth: true },
      }, {
        path: ROUTES.SECTIONS.STORE,
        component: MyStore,
        meta: { requiresAuth: true },
      }, {
        path: ROUTES.LOGIN_REQUIRED,
        component: LoginRequired,
      },
    ],
  }, {
    path: ROUTES.SECTIONS.ADVERTISEMENT,
    component: SingleAdvertisement,
  }, {
    path: ROUTES.SECTIONS.NEGOTIATION,
    component: Chat,
  }, {
    path: ROUTES.LOGIN,
    component: Login,
  },
  {
    path: ROUTES.SECTIONS.PUBLISH_ADVERTISEMENT,
    component: AdvertisementCreation,
  },
];
