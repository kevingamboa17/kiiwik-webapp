export const APP_VERSION = '0.1';

export const ROUTES = {
  HOME: '/',
  LOGIN: '/login',
  LOGIN_REQUIRED: '/loginRequired',
  MAIN: {
    APP: '/main',
  },
  SECTIONS: {
    ADVERTISEMENTS: '/advertisements',
    ADVERTISEMENT: '/advertisements/:id',
    MESSAGES: '/messages',
    NEGOTIATIONS: '/negotiations',
    NEGOTIATION: '/negotiations/:id',
    PUBLISH_ADVERTISEMENT: '/publish_advertisement',
    SETTINGS: '/settings',
    STORE: '/store',
    USERS: '/users',
  },
};

export const URL = {
  API_ENDPOINT: 'https://kiiwik-dev.herokuapp.com/api/',
  ADVERTISEMENTS: 'articles/',
  CHAT_ENTRIES: 'chat_entries/',
  IMAGES: 'images/',
  LOGIN: 'users/',
  NEGOTIATIONS: 'negotiations/',
  OFFERS: 'offers/',
  OPTIONS: {
    UNBLOCK_ADVERTISEMENT: '/publish/',
    BLOCK_ADVERTISEMENT: '/block/',
    BLOCK_USER: '/block/',
    UNBLOCK_USER: '/active/',
  },
  REPORTS: 'reports/',
  USERS: 'users/',
};

export const HEADERS = {
  FB_USER_ID: 'x-auth-fb-user-id',
  FB_ACCESS_TOKEN: 'x-auth-fb-user-token',
};

export const KEYS = {
  LOGGED_IN: 'loggedIn',
  FACEBOOK_ID: 'user-facebook-id',
  FACEBOOK_ACCESS_TOKEN: 'user-facebook-access-token',
  SEARCH_DISTANCE_PREFERENCE: 'user-distance-preference',
  USERNAME: 'username',
  USER_ID: 'user-id',
};

export const CHAT_ENTRIES_TYPES = {
  QUESTION: 'question',
  MESSAGE: 'message',
  OFFER_UPDATE: 'offer_update',
  MEETING: 'meeting',
};
