/* eslint-disable class-methods-use-this */
import Session from '../api/Session';
import { KEYS } from './Constants';

class Auth {
  login(facebookId, accessToken, username, redirect) {
    Session.login(facebookId, accessToken, username)
      .then((result) => {
        if (result) {
          localStorage.setItem(KEYS.LOGGED_IN, 'true');
          localStorage.setItem(KEYS.FACEBOOK_ID, facebookId);
          localStorage.setItem(KEYS.FACEBOOK_ACCESS_TOKEN, accessToken);
          localStorage.setItem(KEYS.SEARCH_DISTANCE_PREFERENCE, 10);
          localStorage.setItem(KEYS.USERNAME, username);
          localStorage.setItem(KEYS.USER_ID, result.data.id);
          redirect();
        }
        return result;
      });
  }
}

export default new Auth();
