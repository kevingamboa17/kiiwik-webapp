/* eslint-disable class-methods-use-this */
import Session from '../api/Session';
import { KEYS } from './Constants';

class Configurations {
  login(facebookId, accessToken, redirect) {
    Session.login(facebookId, accessToken)
      .then((result) => {
        if (result) {
          localStorage.setItem(KEYS.LOGGED_IN, 'true');
          localStorage.setItem(KEYS.FACEBOOK_ID, facebookId);
          localStorage.setItem(KEYS.FACEBOOK_ACCESS_TOKEN, accessToken);
          redirect();
        }
        return result;
      });
  }

  setAdvertisementDistancePreference(preference) {
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(preference)) {
      localStorage.setItem(KEYS.SEARCH_DISTANCE_PREFERENCE, preference);
    }
  }

  getAdvertisementDistancePreference() {
    return localStorage.getItem(KEYS.SEARCH_DISTANCE_PREFERENCE);
  }

  setUsername(username) {
    localStorage.setItem(KEYS.USERNAME, username);
  }

  getUsername() {
    localStorage.getItem(KEYS.USERNAME);
  }
}

export default new Configurations();
