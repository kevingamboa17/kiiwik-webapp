import Vue from 'vue';
import { URL } from '../utils/Constants';
import Session from './Session';

export default {
  uploadImage(filename, content, contentType) {
    const url = `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { filename, content, content_type: contentType },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  uploadImages(images) {
    const uploadedImagesIds = [];
    return new Promise((resolve) => {
      images.forEach((image) => {
        this.uploadImage(
          image.filename,
          image.content,
          image.content_type
        ).then((uploadedImage) => {
          uploadedImagesIds.push(uploadedImage.id);
          if (images.length === uploadedImagesIds.length) resolve();
        });
      });
    });
  },
};
