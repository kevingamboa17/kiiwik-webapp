import Vue from 'vue';
import { URL } from '../utils/Constants';
import Session from './Session';

export default {
  createAdvertisements(advertisement, tags) {
    const url = `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { advertisement: `${advertisement}`, tags: `${tags}` },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (error) => {
        console.log(error);
      });
    });
  },
};
