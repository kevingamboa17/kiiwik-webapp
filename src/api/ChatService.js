import Vue from 'vue';
import { URL, CHAT_ENTRIES_TYPES } from '../utils/Constants';
import Session from './Session';

export default {

  createQuestion(negotiationId, question) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.CHAT_ENTRIES}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { instance_of: `${CHAT_ENTRIES_TYPES.QUESTION}`, question: { body: `${question}` } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  answerQuestion(chatEntryId, answer) {
    const url = `${URL.API_ENDPOINT}${URL.CHAT_ENTRIES}/${chatEntryId}`;
    return new Promise((resolve) => {
      Vue.http.put(
        url,
        { instance_of: `${CHAT_ENTRIES_TYPES.QUESTION}`, question: { answer } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  createMeeting(negotiationId, address, lat, lng, time) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.CHAT_ENTRIES}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { instance_of: `${CHAT_ENTRIES_TYPES.MEETING}`, meeting: { address, location: { lat, lng }, time } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },
};
