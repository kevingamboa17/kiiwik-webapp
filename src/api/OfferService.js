import Vue from 'vue';
import { URL } from '../utils/Constants';
import Session from './Session';

export default {
  createPriceOffer(negotiationId, amount) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.OFFERS}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { amount },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  createProductOffer(negotiationId, amount, description, imagesIds) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.OFFERS}`;
    return new Promise((resolve) => {
      Vue.http.post(
        url,
        { amount, product_to_interchange: { description, images_ids: imagesIds } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  createNegotiation(buyerId, sellerId, advertisementId) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}?buyer`;
    return new Promise((resolve, error) => {
      Vue.http.post(
        url,
        { buyer_id: buyerId, seller_id: sellerId, advertisement_id: advertisementId },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
        error(response);
      });
    });
  },

  replyOffer(negotiationId, offerId, ignored) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.OFFERS}${offerId}`;
    return new Promise((resolve, error) => {
      Vue.http.put(
        url,
        { ignored },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response);
      }, (errorMessage) => {
        console.log(errorMessage);
        error(errorMessage);
      });
    });
  },
};
