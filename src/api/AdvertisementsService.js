import Vue from 'vue';
import { URL } from '../utils/Constants';
import Session from './Session';

export default {
  getAdvertisements(position) {
    const url = this.buildAdvertisementsUrl(position);
    return new Promise((resolve, reject) => {
      Vue.http.get(
        url,
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data.advertisements);
      }, (response) => {
        console.log(response);
        if (response.body.message === 'invalid_facebook_token') {
          Session.logout();
        }
        reject(response.status);
      });
    });
  },

  getSingleAdvertisement(advertisementId) {
    const url = `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}${advertisementId}`;
    return new Promise((resolve) => {
      Vue.http.get(url, { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }).then((response) => {
        resolve(response.data);
      }, (response) => {
        console.log(response);
      });
    });
  },

  buildAdvertisementsUrl(location) {
    if (location !== undefined) {
      return `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}?lat=${location.coords.latitude}&lng=${location.coords.longitude}&distance=10000000&status=active&category=sale`;
    }
    return `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}`;
  },
};
