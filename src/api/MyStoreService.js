import Vue from 'vue';
import { URL } from '../utils/Constants';
import Session from './Session';

export default {
  getOwnAdvertisements(position) {
    const url2 = `${URL.API_ENDPOINT}${URL.ADVERTISEMENTS}?lat=${position.coords.latitude}&lng=${position.coords.longitude}&distance=10000000&status=active&category=sale&user_id=${Session.getFacebookId}`;
    return new Promise((resolve) => {
      Vue.http.get(url2, { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }).then((response) => {
        resolve(response.data.advertisements);
      }, (response) => {
        console.log(response);
      });
    });
  },
};
