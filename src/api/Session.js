/* eslint-disable class-methods-use-this */
import Vue from 'vue';
import { URL, KEYS } from '../utils/Constants';

class Session {
  login(facebookId, accessToken, displayName) {
    const url = `${URL.API_ENDPOINT}${URL.LOGIN}`;
    return Vue.http.post(
      url,
      { token_session: accessToken, facebook_id: facebookId, display_name: displayName },
      { headers: { 'x-auth-fb-user-id': facebookId, 'x-auth-fb-user-token': accessToken } },
    );
  }

  logout() {
    this.localStorage.clear();
  }

  isLoggedIn() {
    return localStorage.getItem(KEYS.LOGGED_IN);
  }

  getFacebookId() {
    if (!this.isLoggedIn()) {
      return '10155187134290917';
    }
    return localStorage.getItem(KEYS.FACEBOOK_ID);
  }

  getUserId() {
    if (!this.isLoggedIn()) {
      return null;
    }
    return localStorage.getItem(KEYS.USER_ID);
  }

  getFacebookAccessToken() {
    if (!this.isLoggedIn()) {
      return '';
    }
    return localStorage.getItem(KEYS.FACEBOOK_ACCESS_TOKEN);
  }

  getUsername() {
    return localStorage.getItem(KEYS.USERNAME);
  }

  getDistancePreference() {
    return localStorage.getItem(KEYS.SEARCH_DISTANCE_PREFERENCE);
  }

  setUsername(username) {
    localStorage.setItem(KEYS.USERNAME, username);
  }

  setDistancePreference(distancePreference) {
    localStorage.setItem(KEYS.SEARCH_DISTANCE_PREFERENCE, distancePreference);
  }
}

export default new Session();
