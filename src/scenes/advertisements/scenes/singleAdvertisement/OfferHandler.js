import OfferService from '../../../../api/OfferService';
import ChatEntriesService from '../../../negotiations/scenes/Messages/ChatEntriesService';
import Session from '../../../../api/Session';

const buyerId = Session.getUserId();
export default {
  sendPriceOffer(sellerId, advertisementId, amount, callback, error) {
    OfferService.createNegotiation(buyerId, sellerId, advertisementId).then((negotiation) => {
      OfferService.createPriceOffer(negotiation.id, amount).then(() => {
        callback();
      }).catch((errorMessage) => {
        error(errorMessage);
      });
    }).catch((errorMessage) => {
      error(errorMessage);
    });
  },

  sendProductOffer(sellerId, advertisementId, amount, description, callback, error) {
    OfferService.createNegotiation(buyerId, sellerId, advertisementId).then((negotiation) => {
      OfferService.createProductOffer(negotiation.id, amount, description, []).then(() => {
        callback();
      }).catch((errorMessage) => {
        error(errorMessage);
      });
    }).catch((errorMessage) => {
      error(errorMessage);
    });
  },

  replyOffer(negotiationId, offerId, ignored, callback, error) {
    OfferService.replyOffer(negotiationId, offerId, ignored).then(() => {
      callback();
    }).catch((errorMessage) => {
      error(errorMessage);
    });
  },

  createQuestion(sellerId, advertisementId, question, callback, error) {
    OfferService.createNegotiation(buyerId, sellerId, advertisementId).then((negotiation) => {
      ChatEntriesService.createQuestion(negotiation.id, question).then(() => {
        callback();
      }).catch((errorMessage) => {
        error(errorMessage);
      });
    }).catch((errorMessage) => {
      error(errorMessage);
    });
  },
};
