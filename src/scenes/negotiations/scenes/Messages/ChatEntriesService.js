import Vue from 'vue';
import { URL } from '../../../../utils/Constants';
import Session from '../../../../api/Session';

function executeApiCall(url) {
  return new Promise((resolve, reject) => {
    Vue.http.get(url, { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }).then((response) => {
      resolve(response.data);
    }, (error) => {
      reject(error);
    });
  });
}

export default {
  getChatEntries(negotiationId) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.CHAT_ENTRIES}`;
    return executeApiCall(url);
  },

  sendMessage(negotiationId, body) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.CHAT_ENTRIES}`;
    return new Promise((resolve, reject) => {
      Vue.http.post(
        url,
        { instance_of: 'message', message: { body } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (error) => {
        reject(error);
      });
    });
  },

  createQuestion(negotiationId, body) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}/${URL.CHAT_ENTRIES}`;
    return new Promise((resolve, reject) => {
      Vue.http.post(
        url,
        { instance_of: 'question', question: { body } },
        { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }
      ).then((response) => {
        resolve(response.data);
      }, (error) => {
        reject(error);
      });
    });
  },
};
