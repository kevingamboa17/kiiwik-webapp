import Vue from 'vue';
import { URL } from '../../../../utils/Constants';
import Session from '../../../../api/Session';

function makeApiCall(url) {
  return new Promise((resolve, reject) => {
    Vue.http.get(url, { headers: { 'x-auth-fb-user-id': Session.getFacebookId(), 'x-auth-fb-user-token': Session.getFacebookAccessToken() } }).then((response) => {
      resolve(response.data);
    }, (error) => {
      console.log('error in response');
      reject(error);
    });
  });
}

export default {
  getNegotiations(role) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}?role=${role}`;
    return makeApiCall(url);
  },

  getNegotiation(negotiationId) {
    const url = `${URL.API_ENDPOINT}${URL.NEGOTIATIONS}${negotiationId}`;
    return makeApiCall(url);
  },
};
