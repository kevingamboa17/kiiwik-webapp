/* eslint-disable import/no-extraneous-dependencies */
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import 'vuetify/dist/vuetify.min.css'; // Ensure you are using css-loader
import 'material-design-icons-iconfont/dist/material-design-icons.css'; // Ensure you are using css-loader
import FBSignInButton from 'vue-facebook-signin-button';
import * as VueGoogleMaps from "vue2-google-maps";
import App from './App.vue';
import Routes from './Routes';
import Session from './api/Session';

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(FBSignInButton);
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(Vuetify, {
  iconfont: 'md',
});

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_GOOGLE_MAPS_KEY,
    libraries: 'places',
  },
});

const router = new VueRouter({
  mode: 'history',
  routes: Routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!Session.isLoggedIn()) {
      next({
        path: '/loginRequired',
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');

const prod = process.env.NODE_ENV === 'production';
const shouldSW = 'serviceWorker' in navigator && prod;
if (shouldSW) {
  navigator.serviceWorker.register('/service-worker.js').then(() => {
    console.log('Service Worker Registered!');
  });
}
